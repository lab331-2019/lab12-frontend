import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { CourseListComponent } from './course/course-list/course-list.component';
import { CourseAddComponent } from './course/course-add/course-add.component';
import { CourseInfoComponent } from './course/course-info/course-info.component';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  {
    path: 'course/list',
    component: CourseListComponent
  },
  {
    path: 'course/add',
    component: CourseAddComponent
  },
  { path: 'course/:id', component: CourseInfoComponent },
  { path: '**', component: FileNotFoundComponent }

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
