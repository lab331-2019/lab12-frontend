import { Observable } from '../../../node_modules/rxjs';
import Student from '../entity/student';
import  Course  from '../entity/course';
export abstract class CourseService {
  save(model: Course) {
    throw new Error("Method not implemented.");
  }
    abstract getCourses(): Observable<Course[]>;
    abstract getCourse(id: number): Observable<Course>;
    abstract saveCourse(course: Course): Observable<Course>;
}
