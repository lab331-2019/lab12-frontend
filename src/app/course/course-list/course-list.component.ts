import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { CourseListDataSource, CourseListItem } from './course-list-datasource';
import { CourseService } from 'src/app/service/course-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  dataSource: CourseListDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['courseId', 'name', 'content', 'lecturer','view'];

  constructor(private courseService: CourseService,private router: Router) { }
  ngOnInit() {
    this.courseService.getCourses().subscribe(
      courses => {
        this.dataSource = new CourseListDataSource(this.paginator, this.sort);
        this.dataSource.data = courses;
      });

  }
  routeToCourseInfo(courseId: number) {
    this.router.navigate(['course',courseId]);
  }
}
